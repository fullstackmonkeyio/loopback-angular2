#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$(cd ${SCRIPT_DIR}/..; pwd)

${SCRIPT_DIR}/exec.sh server "node_modules/typescript/bin/tsc $@"
