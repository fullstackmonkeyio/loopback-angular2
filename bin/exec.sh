#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$(cd ${SCRIPT_DIR}/..; pwd)

CONTAINER="${1}"
shift
CMD="cd /usr/src/app; ${@}"

docker-compose exec ${CONTAINER} bash -c "${CMD}"
