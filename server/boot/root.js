"use strict";
function root(server) {
    var router = server.loopback.Router();
    router.get('/', server.loopback.status());
    server.use(router);
}
module.exports = root;
//# sourceMappingURL=root.js.map