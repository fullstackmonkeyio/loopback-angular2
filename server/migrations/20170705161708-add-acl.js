'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {

  return db.createTable('ACL', {
    id: { type: 'int', length: 11, primaryKey: true, autoIncrement: true },
    model: { type: 'string', length: 512, notNull: true },
    property: { type: 'string', length: 512, notNull: true },
    accessType: { type: 'string', length: 512, notNull: true },
    permission: { type: 'string', length: 512, notNull: true },
    principalType: { type: 'string', length: 512, notNull: true },
    principalId: { type: 'string', length: 512, notNull: true }
  });

};

exports.down = function(db) {
  return db.dropTable('ACL');
};

exports._meta = {
  "version": 1
};
