'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {

  return db.createTable('AccessToken', {
    id: { type: 'string', length: 255, primaryKey: true },
    ttl: { type: 'int', length: 11, notNull: true },
    created: { type: 'datetime', notNull: true },
    userId: { type: 'int', length: 11, notNull: true }
  });

};

exports.down = function(db) {
  return db.dropTable('AccessToken');
};

exports._meta = {
  "version": 1
};
