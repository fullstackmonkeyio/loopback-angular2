'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {

  return db.createTable('Role', {
    id: { type: 'int', length: 11, primaryKey: true, autoIncrement: true },
    name: { type: 'string', length: 512, notNull: false },
    description: { type: 'string', length: 512, notNull: true },
    created: { type: 'datetime', notNull: true },
    modified: { type: 'datetime', notNull: true }
  });

};

exports.down = function(db) {
  return db.dropTable('Role');
};

exports._meta = {
  "version": 1
};
