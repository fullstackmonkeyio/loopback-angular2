'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {

  return db.createTable('User', {
    id: { type: 'int', length: 11, primaryKey: true, autoIncrement: true },
    realm: { type: 'string', length: 512, notNull: true },
    username: { type: 'string', length: 512, notNull: true },
    password: { type: 'string', length: 512, notNull: false },
    email: { type: 'string', length: 512, notNull: false },
    emailVerified: { type: 'boolean', notNull: true },
    verificationToken: { type: 'string', length: 512, notNull: false }
  });

};

exports.down = function(db) {
  return db.dropTable('User');
};

exports._meta = {
  "version": 1
};
