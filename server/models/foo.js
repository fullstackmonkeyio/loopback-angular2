"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("@mean-expert/model");
let Foo = class Foo {
    constructor(model) {
        this.model = model;
    }
    access(ctx, next) {
        console.log('foo: access');
        next();
    }
    persist(ctx, next) {
        console.log('foo: persist');
        next();
    }
    beforeSave(ctx, next) {
        console.log('foo: before Save');
        next();
    }
    afterSave(ctx, next) {
        console.log('foo: after Save');
        next();
    }
    beforeDelete(ctx, next) {
        console.log('foo: before Delete');
        next();
    }
    afterDelete(ctx, next) {
        console.log('foo: after Delete');
        next();
    }
};
Foo = __decorate([
    model_1.Model({
        hooks: {
            access: { name: 'access', type: 'operation' },
            persist: { name: 'persist', type: 'operation' },
            afterSave: { name: 'after save', type: 'operation' },
            beforeSave: { name: 'before save', type: 'operation' },
            beforeDelete: { name: 'before delete', type: 'operation' },
            afterDelete: { name: 'after delete', type: 'operation' }
        },
        remotes: {}
    })
], Foo);
module.exports = Foo;
//# sourceMappingURL=foo.js.map