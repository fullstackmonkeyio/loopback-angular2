import {Model} from "@mean-expert/model";

/**
 * @module Foo
 * @description
 **/
@Model({
  hooks: {
    access: {name: 'access', type: 'operation'},
    persist: {name: 'persist', type: 'operation'},
    afterSave: {name: 'after save', type: 'operation'},
    beforeSave: {name: 'before save', type: 'operation'},
    beforeDelete: {name: 'before delete', type: 'operation'},
    afterDelete: {name: 'after delete', type: 'operation'}
  },
  remotes: {
  }
})

class Foo {

  constructor(public model: any) {
  }

  access(ctx: any, next: Function): void {
    console.log('foo: access');
    next();
  }

  persist(ctx: any, next: Function): void {
    console.log('foo: persist');
    next();
  }

  beforeSave(ctx: any, next: Function): void {
    console.log('foo: before Save');
    next();
  }

  afterSave(ctx: any, next: Function): void {
    console.log('foo: after Save');
    next();
  }

  beforeDelete(ctx: any, next: Function): void {
    console.log('foo: before Delete');
    next();
  }

  afterDelete(ctx: any, next: Function): void {
    console.log('foo: after Delete');
    next();
  }

}

module.exports = Foo;
