"use strict";
var loopback = require('loopback'), boot = require('loopback-boot'), app = loopback(), module, __dirname;
app.start = function () {
    return app.listen(function () {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
    });
};
boot(app, __dirname, function (err) {
    if (err)
        throw err;
    if (require.main === module)
        app.start();
});
module.exports = app;
//# sourceMappingURL=server.js.map