/* tslint:disable */

declare var Object: any;
export interface FooInterface {
  "id"?: number;
}

export class Foo implements FooInterface {
  "id": number;
  constructor(data?: FooInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Foo`.
   */
  public static getModelName() {
    return "Foo";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Foo for dynamic purposes.
  **/
  public static factory(data: FooInterface): Foo{
    return new Foo(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Foo',
      plural: 'foo',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
